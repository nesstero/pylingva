[![PyPI version](https://badge.fury.io/py/pylingva.svg)](https://badge.fury.io/py/pylingva)
# Pylingva
Simple translator using [Lingva Translate](https://lingva.ml/) API

# Install
```
pip install pylingva
```

# Usage
## CLI
```shell
$ translate -h

██████╗ ██╗   ██╗██╗     ██╗███╗   ██╗ ██████╗ ██╗   ██╗ █████╗
██╔══██╗╚██╗ ██╔╝██║     ██║████╗  ██║██╔════╝ ██║   ██║██╔══██╗
██████╔╝ ╚████╔╝ ██║     ██║██╔██╗ ██║██║  ███╗██║   ██║███████║
██╔═══╝   ╚██╔╝  ██║     ██║██║╚██╗██║██║   ██║╚██╗ ██╔╝██╔══██║
██║        ██║   ███████╗██║██║ ╚████║╚██████╔╝ ╚████╔╝ ██║  ██║
╚═╝        ╚═╝   ╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝
Simple translator tool.


Options:
  -h, --help             Display this message
  -u, --url              Server URL of an instance
  -s, --source           Source Language to translate
  -t, --target           Target Language to translate
  -txt, --text           Text to translate
  -ll, --list-languages  List Languages support
  -f, --file             Path file .txt to translate
  -o, --output           Output file translation result

$ translate -s auto -t id -txt "How are You ?"
 Apa kabarmu ?

$ translate -s auto -t id -txt "How are You ?" -u https://lingva.ml
 Apa kabarmu ?

$ translate -s auto -t id -txt "How are You ?" -o output.txt
 Translation result saved in output.txt

$ translate -s auto -t id -f input.txt -o output.txt
 Translation result saved in output.txt
```
## Python
### List languages
```python
from pylingva import pylingva
trans = pylingva()
list_languages = trans.languages()
print(list_languages)
```
**Output:**
```
{'Detect': 'auto', 'Afrikaans': 'af', 'Albanian': 'sq', 'Amharic': 'am', 'Arabic': 'ar', 'Armenian': 'hy', 'Azerbaijani': 'az', 'Basque': 'eu', 'Belarusian': 'be', 'Bengali': 'bn', 'Bosnian': 'bs', 'Bulgarian': 'bg', 'Catalan': 'ca', 'Cebuano': 'ceb', 'Chichewa': 'ny', 'Chinese': 'zh', 'Chinese (Traditional)': 'zh_HANT', 'Corsican': 'co', 'Croatian': 'hr', 'Czech': 'cs', 'Danish': 'da', 'Dutch': 'nl', 'English': 'en', 'Esperanto': 'eo', 'Estonian': 'et', 'Filipino': 'tl', 'Finnish': 'fi', 'French': 'fr', 'Frisian': 'fy', 'Galician': 'gl', 'Georgian': 'ka', 'German': 'de', 'Greek': 'el', 'Gujarati': 'gu', 'Haitian Creole': 'ht', 'Hausa': 'ha', 'Hawaiian': 'haw', 'Hebrew': 'iw', 'Hindi': 'hi', 'Hmong': 'hmn', 'Hungarian': 'hu', 'Icelandic': 'is', 'Igbo': 'ig', 'Indonesian': 'id', 'Irish': 'ga', 'Italian': 'it', 'Japanese': 'ja', 'Javanese': 'jw', 'Kannada': 'kn', 'Kazakh': 'kk', 'Khmer': 'km', 'Kinyarwanda': 'rw', 'Korean': 'ko', 'Kurdish (Kurmanji)': 'ku', 'Kyrgyz': 'ky', 'Lao': 'lo', 'Latin': 'la', 'Latvian': 'lv', 'Lithuanian': 'lt', 'Luxembourgish': 'lb', 'Macedonian': 'mk', 'Malagasy': 'mg', 'Malay': 'ms', 'Malayalam': 'ml', 'Maltese': 'mt', 'Maori': 'mi', 'Marathi': 'mr', 'Mongolian': 'mn', 'Myanmar (Burmese)': 'my', 'Nepali': 'ne', 'Norwegian': 'no', 'Odia (Oriya)': 'or', 'Pashto': 'ps', 'Persian': 'fa', 'Polish': 'pl', 'Portuguese': 'pt', 'Punjabi': 'pa', 'Romanian': 'ro', 'Russian': 'ru', 'Samoan': 'sm', 'Scots Gaelic': 'gd', 'Serbian': 'sr', 'Sesotho': 'st', 'Shona': 'sn', 'Sindhi': 'sd', 'Sinhala': 'si', 'Slovak': 'sk', 'Slovenian': 'sl', 'Somali': 'so', 'Spanish': 'es', 'Sundanese': 'su', 'Swahili': 'sw', 'Swedish': 'sv', 'Tajik': 'tg', 'Tamil': 'ta', 'Tatar': 'tt', 'Telugu': 'te', 'Thai': 'th', 'Turkish': 'tr', 'Turkmen': 'tk', 'Ukrainian': 'uk', 'Urdu': 'ur', 'Uyghur': 'ug', 'Uzbek': 'uz', 'Vietnamese': 'vi', 'Welsh': 'cy', 'Xhosa': 'xh', 'Yiddish': 'yi', 'Yoruba': 'yo', 'Zulu': 'zu'}
```

### Translate
```python
from pylingva import pylingva
trans = pylingva()
txt = "Halo apa kabar ?"
translate = trans.translate("auto", "en", txt)
print(translate)
```
**Output**:
```
Hello, how are you ?
```
