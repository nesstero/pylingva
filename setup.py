#!/usr/bin/env python
from setuptools import find_packages, setup

with open("README.md", "r") as des:
    l_desc = des.read()

setup(
    name='pylingva',
    packages=find_packages(),
    version='0.1.2.2',
    entry_points={'console_scripts': ['translate = pylingva.cli:translate']},
    description='Simple translator using Lingva Translate API',
    url='https://gitlab.com/nesstero/pylingva',
    license='MIT',
    author='nestero',
    author_email='nestero@mail.com',
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
